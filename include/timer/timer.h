/*
 * timer.h
 *
 *  Created on: Dec 4, 2020
 *      Author: ZKSWE Develop Team
 */

#ifndef _TIMER_TIMER_H_
#define _TIMER_TIMER_H_

#include "context/common.h"

bool timer_init();
void timer_deinit();
void timer_check();

#endif /* _TIMER_TIMER_H_ */
