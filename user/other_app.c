/*
 * other_app.c
 *
 *  Created on: Jul 20, 2020
 *      Author: laiyj
 */

#include "main.h"
#include "common.h"
#include "devmodule.h"
#include "uicommon.h"

uint16_t AD_Value = 0;
extern ADC_HandleTypeDef hadc1;
extern S_MCU_DATA* getMcudata();

//查询获取adc的值
uint16_t get_adcvalue() {
	uint16_t adcbuff;
	HAL_ADC_Start(&hadc1);	//启动ADC转换
	HAL_ADC_PollForConversion(&hadc1, 1);	//等待转换完成
	adcbuff = HAL_ADC_GetValue(&hadc1);	//读取ADC转换数据
	//adcbuff = adcbuff / 41;//(uint16_t)((float)AD_Value / 40.95);	//转换为0-100
	return adcbuff;
}

static uint16_t adcbuff[8]={0};
static uint16_t adctemp16;
void initAirAdc(){
    uint8_t size;
    for(size =0;size < 8;size++){
        adcbuff[size] = get_adcvalue();
        adctemp16 += adcbuff[size];
    }
 	adctemp16 = adctemp16>>3;
    //return adctemp16;
}

uint16_t getAiradc(){
    uint8_t size;
    for(size =0;size < 7;size++){
        adcbuff[size] = adcbuff[size+1];
    }
    adcbuff[7] = get_adcvalue();
 	adctemp16 = 0;
    for(size =0;size < 8;size++){
        adctemp16 += adcbuff[size];
    }
 	adctemp16 = adctemp16>>3;
    return adctemp16;
}


void procOtherMsg(int msgtype,int p1,int p2) {
	//S_MCU_DATA* mcudata = getMcudata();
	switch(msgtype) {
		case DEVMSG_TIMER10ms:

			break;
		case DEVMSG_TIMER50ms:
//			if(mcudata->BackLightOn_Off == UNDISPLAY) {
//
//			} else if(mcudata->BackLightOn_Off == DISPLAY) {
//
//			}
			AD_Value = getAiradc();
			break;
		case DEVMSG_TIMER100ms:

			break;
		default :
			break;
	}
}


const S_DEV otherapp = {
		initAirAdc,
		NULL,
		NULL,
		NULL,
		procOtherMsg
};
