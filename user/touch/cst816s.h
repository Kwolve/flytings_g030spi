/*
 * cst816s.h
 *
 *  Created on: May 24, 2021
 *      Author: Admin
 */

#ifndef USER_CST816S_H_
#define USER_CST816S_H_

#include "main.h"
#include "stdio.h"
#include "string.h"
#include "common.h"

typedef short s16;
typedef int s32;
typedef char s8;


#define HYN_MAX_POINTS                          1//10
#define HYN_MAX_ID                              0x0F
#define HYN_TOUCH_STEP                          6//6
#define HYN_FACE_DETECT_POS                     1
#define HYN_TOUCH_X_H_POS                       3
#define HYN_TOUCH_X_L_POS                       4
#define HYN_TOUCH_Y_H_POS                       5
#define HYN_TOUCH_Y_L_POS                       6
#define HYN_TOUCH_EVENT_POS                     3
#define HYN_TOUCH_ID_POS                        5
#define FT_TOUCH_POINT_NUM                      2
#define HYN_TOUCH_XY_POS                        7
#define HYN_TOUCH_MISC                          8
#define POINT_READ_BUF                          (3 + HYN_TOUCH_STEP * HYN_MAX_POINTS)
#define TPD_DELAY                               (2*HZ/100)


/*****************************************************************************
* Private enumerations, structures and unions using typedef
*****************************************************************************/
struct touch_info
{
    int y[HYN_MAX_POINTS];
    int x[HYN_MAX_POINTS];
    int p[HYN_MAX_POINTS];
    int id[HYN_MAX_POINTS];
    int count;
};


/*touch event info*/
struct ts_event
{
    u16 au16_x[HYN_MAX_POINTS];               /* x coordinate */
    u16 au16_y[HYN_MAX_POINTS];               /* y coordinate */
    u8 au8_touch_event[HYN_MAX_POINTS];       /* touch event: 0 -- down; 1-- up; 2 -- contact */
    u8 au8_finger_id[HYN_MAX_POINTS];         /* touch ID */
    u16 pressure[HYN_MAX_POINTS];
    u16 area[HYN_MAX_POINTS];
    u8 touch_point;
    int touchs;
    u8 touch_point_num;
};

void touch_init(void);
int read_touch_data(struct ts_event *data);

#endif /* USER_CST816S_H_ */
