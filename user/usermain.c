/*
 * usermain.c
 *
 *  Created on: Jun 12, 2020
 *      Author: 钟广�???
 */


#include "main.h"
#include "devmodule.h"

#include "context/guicontext.h"
#include "event/event.h"
#include "timer/timer.h"
#include "graphics/video.h"
#define USER_RTP	0
#if USER_RTP
const S_DEV* tpres;	//LCD通信串口
#endif
extern TIM_HandleTypeDef htim3;;

const S_DEV* uartdev;		//flash烧写串口


const S_DEV* uart1dev;	//LCD通信串口

static uint32_t tick5ms = 0;
static uint32_t tick10ms = 0;
static uint32_t tick50ms = 0;
static uint32_t tick100ms = 0;
static uint32_t tick500ms = 0;
static uint32_t tick1s = 0;
static uint32_t tick60s = 0;
static int proframe = 0;
uint8_t g_te;
extern const flythings_flash	m_flash;
const flythings_callback_t cbs={
	.init_event = event_init,
	.init_timer = timer_init,
	.init_video = video_init,
	.deinit_event = event_deinit,
	.deinit_timer = timer_deinit,
	.deinit_video = video_deinit,
	.cb_on_pageopen = on_page_open,
	.cb_on_animation_play_end = on_animation_play_end,
	.cb_on_button_touch_event = on_button_touch_event,
	.cb_on_progress_changed = on_progress_changed,
	.cb_on_timer = on_timer,
	.poll_timer = timer_check,
	.poll_event = event_poll,
	.flip_video = video_flip,
	.fly_debug = printf
};

#define LED_ON() 	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_RESET)

void user_init(){
	uartdev = loadDev(&dev_uartupgrade);
 	printf("load uart \r\n");
	printf("led on\n");
	tick10ms = HAL_GetTick();
	tick50ms = HAL_GetTick();
	tick100ms = HAL_GetTick();
	tick500ms = HAL_GetTick();
	tick1s = HAL_GetTick();
	tick60s = HAL_GetTick();
	tick5ms = HAL_GetTick();

	m_flash.flash_init();
	video_info_t vi = { 240, 240, 16, 0 };
	if(gui_context_init(&vi,&cbs,&m_flash)){
		gui_context_open_page(0);
		LED_ON();
		printf("open page 0");
	}
	
}

uint8_t testmcudataflag = 0;	//for test
void user_loop(){
	static uint32_t systick = 0;
	gui_context_run();
	systick = HAL_GetTick();
	if((systick-tick10ms) > 10){
	  tick10ms = systick;
 	  uartdev->procMsg(DEVMSG_TIMER10ms,0,0);
	}
	if((systick-tick5ms) > 5){
	  tick5ms = systick;
		procKey();
	}
	if((systick-tick50ms) > 50){
	   tick50ms = systick;
	   uartdev->procMsg(DEVMSG_TIMER50ms,0,0);
	}
	if((systick - tick100ms) >= 100){
		tick100ms = systick;
		uartdev->procMsg(DEVMSG_TIMER100ms,0,0);
	}
	if((systick - tick500ms) >= 500){
		tick500ms = systick;
	}
	if((systick - tick1s) >= 1000){
		tick1s = systick;
	}
	if((systick - tick60s) >= (60000*10)){//10000){//
		tick60s = systick;
	}
}