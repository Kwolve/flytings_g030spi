#ifndef INPUT_H
#define INPUT_H

typedef enum{
    KEY_LIGHT1= 0,
	KEY_LIGHT2,
	KEY_LIGHT3,
	KEY_LIGHT4,
	KEY_ENCODE_NEXT,
	KEY_ENCODE_PRE,
    KEY_NOKEY = 0xff
}E_KEY;

#define KEYST_UP    0
#define KEYST_DOWN 1
#define KEYST_KEEP  2
#define KEST_KEEP_1S   3
#define KEYST_KEEP_3S   4
#define KEYST_KEEP_5S   5

typedef struct{
    uint8_t keystatus;
    uint8_t keyvalue;
}S_INPUT_KEY;

#endif 
