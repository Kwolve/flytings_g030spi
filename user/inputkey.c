#include "main.h"
#include "common.h"
#include "input.h"
#include "cst816s.h"

u8 readKey(){
//   if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13) == 0) return KEY_LIGHT1;		//k1
//   if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_14) == 0) return KEY_LIGHT2;		//k2
//   if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_15) == 0) return KEY_LIGHT3;		//k3
	struct ts_event tp_info = { 0 };
	
	int ret = read_touch_data(&tp_info);
	if(ret < 0) return KEY_NOKEY;
	//printf("tp_info.touchs: %d\n", tp_info.touchs);	//滑动
	//滑动动作
	if(tp_info.touchs == 3) return KEY_LIGHT1;		//下滑
	if(tp_info.touchs == 2) return KEY_LIGHT2;		//左滑
	if(tp_info.touchs == 1) return KEY_LIGHT3;		//右滑
	if(tp_info.touchs == 4) return KEY_LIGHT4;		//上滑
	
   return KEY_NOKEY;
}
extern void ui_proc_keyevent(S_INPUT_KEY* keyvalue);
static u8 keystatus = 0;
static u8 keyValue = KEY_NOKEY;
static unsigned short keypressTimer = 0;
void procEncode();
void procKey(){
    static u8 lastkey = 0;
    procEncode();
    u8 value = readKey();
    if(lastkey != value){
        lastkey = value;
        printf("scan key:%x\r\n",value);
		if(lastkey == KEY_NOKEY){
			S_INPUT_KEY skey;
			keystatus = KEYST_UP;
			skey.keystatus = keystatus;
			skey.keyvalue = keyValue;
			printf("proc key up:%d\r\n",keyValue);
			keyValue = KEY_NOKEY;
			keypressTimer = 0;
            ui_proc_keyevent(&skey);
			//guiProcKey(&skey);
			return;
		}
        if(keystatus == KEYST_UP){
			S_INPUT_KEY skey;
	        keyValue = value;
			keystatus = KEYST_DOWN;
			skey.keystatus = KEYST_DOWN;
			skey.keyvalue = keyValue;
			keypressTimer = 0;
			printf("proc key down:%d\r\n",keyValue);
            ui_proc_keyevent(&skey);
			//guiProcKey(&skey);
		}

    }else if(keystatus == KEYST_DOWN){
        keypressTimer++;
        if(keypressTimer == (1000/10)){
            S_INPUT_KEY skey;
            skey.keystatus = KEST_KEEP_1S;
            skey.keyvalue = keyValue;
            printf("proc key keep 1s:%d\r\n",keyValue);
            ui_proc_keyevent(&skey);
           // guiProcKey(&skey);
        }else if(keypressTimer == (3000/10)){
            S_INPUT_KEY skey;
            skey.keystatus = KEYST_KEEP_3S;
            skey.keyvalue = keyValue;
            printf("proc key keep 3s:%d\r\n",keyValue);
            ui_proc_keyevent(&skey);
            //guiProcKey(&skey); 
        }else if(keypressTimer == (5000/10)){
            S_INPUT_KEY skey;
            skey.keystatus = KEYST_KEEP_5S;
            skey.keyvalue = keyValue;
            printf("proc key keep 5s:%d\r\n",keyValue);
            ui_proc_keyevent(&skey);
            //guiProcKey(&skey); 
        }
    }

}


uint8_t EncodeStatus;
uint8_t EncodeStatusOld;

void HAL_GPIO_EXTI_Falling_Callback(uint16_t GPIO_Pin)
{

	  uint8_t encode = 0;
	  if(GPIOA->IDR&GPIO_PIN_11)  encode |= 0x01;
	  if(GPIOA->IDR&GPIO_PIN_12)  encode |= 0x02;
	  if((EncodeStatus&0x03) != (encode&0x03)){
		  EncodeStatus <<= 2;
		  EncodeStatus |= encode&0x03;
	  }
	  
	if(GPIO_Pin == GPIO_PIN_1) {
		//通知有触摸按下
	}
}
void HAL_GPIO_EXTI_Rising_Callback(uint16_t GPIO_Pin)
{

	  uint8_t encode = 0;
	  if(GPIOA->IDR&GPIO_PIN_11)  encode |= 0x01;
	  if(GPIOA->IDR&GPIO_PIN_12)  encode |= 0x02;
	  if((EncodeStatus&0x03) != (encode&0x03)){
		  EncodeStatus <<= 2;
		  EncodeStatus |= encode&0x03;
	  }
}

uint8_t dislightflag;
void procEncode(){
	static uint8_t filter1 = 1; //过滤掉一次按键
	static uint8_t filter2 = 1;
   if(EncodeStatusOld != EncodeStatus){
	   printf("encode stats:%x\n",EncodeStatus&0xf);
	   EncodeStatusOld = EncodeStatus;
	   S_INPUT_KEY skey;
	   switch(EncodeStatus&0xf){
	   case 7:
	   case 8:
		   skey.keystatus = KEYST_DOWN;
		   skey.keyvalue = KEY_ENCODE_PRE;
		   printf("proc encode key:%d\r\n",skey.keyvalue);
		   filter2 = 1;
		   if(filter1 > 0 && dislightflag == 0) {
			   filter1--;
		   } else {
			   filter1 = 1;
			  // guiProcKey(&skey);
		   }
            ui_proc_keyevent(&skey);
		   break;
	   case 4:
	   case 0x0b:
		   skey.keystatus = KEYST_DOWN;
		   skey.keyvalue = KEY_ENCODE_NEXT;
		   printf("proc encode key:%d\r\n",skey.keyvalue);
		   filter1 = 1;
		   if(filter2 > 0 && dislightflag == 0) {
			   filter2--;
		   } else {
			   filter2 = 1;
			  // guiProcKey(&skey);
		   }
            ui_proc_keyevent(&skey);
		   break;
	   }
   }
}
