/*
 * video.h
 *
 *  Created on: Nov 29, 2020
 *      Author: ZKSWE Develop Team
 */

#ifndef _GRAPHICS_VIDEO_H_
#define _GRAPHICS_VIDEO_H_

#include "context/common.h"

bool video_init();
void video_deinit();
void video_flip();

#endif /* _GRAPHICS_VIDEO_H_ */
