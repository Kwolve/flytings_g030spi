/*
 * SDL_video.c
 *
 *  Created on: Nov 29, 2020
 *      Author: ZKSWE Develop Team
 */
#include "main.h"
#include "video.h"

#if 1//def SWM_191

extern TIM_HandleTypeDef htim3;
extern SPI_HandleTypeDef hspi2;

#define RST_LOW()	HAL_GPIO_WritePin(GPIOA,GPIO_PIN_8,GPIO_PIN_RESET)
#define RST_HI()	HAL_GPIO_WritePin(GPIOA,GPIO_PIN_8,GPIO_PIN_SET)

#define LED_OFF() 	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_SET)
#define LED_ON() 	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_6,GPIO_PIN_RESET)
#define LCD_ON() 	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_7,GPIO_PIN_RESET)

#define SWAP(x) (x>>8)|(x<<8)


//RS
#define SET_CMD()	(GPIOB->BRR = GPIO_PIN_13)//HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_RESET)
#define SET_DATA()	(GPIOB->BSRR = GPIO_PIN_13)//HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_SET)


#define CLR_CS()	(GPIOB->BRR = GPIO_PIN_12)//HAL_GPIO_WritePin(GPIOC,GPIO_PIN_3,GPIO_PIN_RESET)
#define SET_CS()	(GPIOB->BSRR = GPIO_PIN_12)//HAL_GPIO_WritePin(GPIOC,GPIO_PIN_3,GPIO_PIN_SET)

#define SET_RD()	//(GPIOC->BSRR = GPIO_PIN_6)//HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_RESET)
#define CLR_RD()	//(GPIOC->BRR = GPIO_PIN_6)//HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_SET)


#define SWAP(x) (x>>8)|(x<<8)


#define TFT_SPI_Write_Byte(num)		{\
	while(((SPI2->SR) & (SPI_FLAG_TXE)) != (SPI_FLAG_TXE));\
	 *((__IO uint8_t*)&SPI2->DR) = num;\
	 while(((SPI2->SR) & SPI_FLAG_BSY) != RESET);}


#define LCD_WritemutileData16_fast(data)  {\
	while(((SPI2->SR) & (SPI_FLAG_TXE)) != (SPI_FLAG_TXE));\
	SPI2->DR =  SWAP(data);\
}

void LCD_startWriteMutileData(){
	SET_DATA();
	CLR_CS();
}

void LCD_WritemutileData(uint8_t data){
	TFT_SPI_Write_Byte(data);
}

void LCD_endWriteMutileData(){
	SET_CS();
}

void LCD_WR_REG(uint8_t CMD)
{
	CLR_CS();
	SET_CMD();
	TFT_SPI_Write_Byte(CMD);
	SET_CS();
}

void LCD_WR_DATA(uint8_t DATA)
{
	CLR_CS();
	SET_DATA();
	TFT_SPI_Write_Byte(DATA);
	SET_CS();
}
void LCD_WR_DATA16(uint16_t DATA)
{
	SET_DATA();
	CLR_CS();

	//TFT_SPI_Write_16(DATA);
	TFT_SPI_Write_Byte(DATA>>8);
	TFT_SPI_Write_Byte(DATA&0xff);
	SET_CS();
}
#define LCD_ILI9341_CMD 		LCD_WR_REG
#define LCD_ILI9341_Parameter 	LCD_WR_DATA
#define delay_ms 				HAL_Delay
#define Delay			HAL_Delay
#define WriteData	LCD_WR_DATA
#define WriteComm LCD_WR_REG

#define LCD_DIR 0	//����
#define LCD_width 	240
#define LCD_height	320
#define LCD_wramcmd 0X2C
#define LCD_setxcmd 0X2A
#define LCD_setycmd 0X2B
void LCD_Clear(uint16_t color)
{
}  

bool video_init() {
	
	__HAL_SPI_ENABLE(&hspi2);
    LED_OFF();
	RST_LOW();
	LCD_ON();
	delay_ms(20);
	RST_HI();
	delay_ms(20);
	SET_RD();
	LCD_WR_REG(0xEF);
	LCD_WR_REG(0xEB);
	LCD_WR_DATA(0x14);

	LCD_WR_REG(0xFE);
	LCD_WR_REG(0xEF);

	LCD_WR_REG(0xEB);
	LCD_WR_DATA(0x14);

	LCD_WR_REG(0x84);
	LCD_WR_DATA(0x40);

	LCD_WR_REG(0x85);
	LCD_WR_DATA(0xFF);

	LCD_WR_REG(0x86);
	LCD_WR_DATA(0xFF);

	LCD_WR_REG(0x87);
	LCD_WR_DATA(0xFF);

	LCD_WR_REG(0x88);
	LCD_WR_DATA(0x0A);

	LCD_WR_REG(0x89);
	LCD_WR_DATA(0x21);

	LCD_WR_REG(0x8A);
	LCD_WR_DATA(0x00);

	LCD_WR_REG(0x8B);
	LCD_WR_DATA(0x80);

	LCD_WR_REG(0x8C);
	LCD_WR_DATA(0x01);

	LCD_WR_REG(0x8D);
	LCD_WR_DATA(0x01);

	LCD_WR_REG(0x8E);
	LCD_WR_DATA(0xFF);

	LCD_WR_REG(0x8F);
	LCD_WR_DATA(0xFF);


	LCD_WR_REG(0xB6);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x20);

	LCD_WR_REG(0x36);
//	if(USE_HORIZONTAL==0)LCD_WR_DATA(0x08);
//	else if(USE_HORIZONTAL==1)LCD_WR_DATA(0xC8);
//	else if(USE_HORIZONTAL==2)LCD_WR_DATA(0x68);
//	else
	LCD_WR_DATA(0x08);

	LCD_WR_REG(0x3A);
	LCD_WR_DATA(0x05);


	LCD_WR_REG(0x90);
	LCD_WR_DATA(0x08);
	LCD_WR_DATA(0x08);
	LCD_WR_DATA(0x08);
	LCD_WR_DATA(0x08);

	LCD_WR_REG(0xBD);
	LCD_WR_DATA(0x06);

	LCD_WR_REG(0xBC);
	LCD_WR_DATA(0x00);

	LCD_WR_REG(0xFF);
	LCD_WR_DATA(0x60);
	LCD_WR_DATA(0x01);
	LCD_WR_DATA(0x04);

	LCD_WR_REG(0xC3);
	LCD_WR_DATA(0x13);
	LCD_WR_REG(0xC4);
	LCD_WR_DATA(0x13);

	LCD_WR_REG(0xC9);
	LCD_WR_DATA(0x22);

	LCD_WR_REG(0xBE);
	LCD_WR_DATA(0x11);

	LCD_WR_REG(0xE1);
	LCD_WR_DATA(0x10);
	LCD_WR_DATA(0x0E);

	LCD_WR_REG(0xDF);
	LCD_WR_DATA(0x21);
	LCD_WR_DATA(0x0c);
	LCD_WR_DATA(0x02);

	LCD_WR_REG(0xF0);
	LCD_WR_DATA(0x45);
	LCD_WR_DATA(0x09);
	LCD_WR_DATA(0x08);
	LCD_WR_DATA(0x08);
	LCD_WR_DATA(0x26);
	LCD_WR_DATA(0x2A);

	LCD_WR_REG(0xF1);
	LCD_WR_DATA(0x43);
	LCD_WR_DATA(0x70);
	LCD_WR_DATA(0x72);
	LCD_WR_DATA(0x36);
	LCD_WR_DATA(0x37);
	LCD_WR_DATA(0x6F);


	LCD_WR_REG(0xF2);
	LCD_WR_DATA(0x45);
	LCD_WR_DATA(0x09);
	LCD_WR_DATA(0x08);
	LCD_WR_DATA(0x08);
	LCD_WR_DATA(0x26);
	LCD_WR_DATA(0x2A);

	LCD_WR_REG(0xF3);
	LCD_WR_DATA(0x43);
	LCD_WR_DATA(0x70);
	LCD_WR_DATA(0x72);
	LCD_WR_DATA(0x36);
	LCD_WR_DATA(0x37);
	LCD_WR_DATA(0x6F);

	LCD_WR_REG(0xED);
	LCD_WR_DATA(0x1B);
	LCD_WR_DATA(0x0B);

	LCD_WR_REG(0xAE);
	LCD_WR_DATA(0x77);

	LCD_WR_REG(0xCD);
	LCD_WR_DATA(0x63);


	LCD_WR_REG(0x70);
	LCD_WR_DATA(0x07);
	LCD_WR_DATA(0x07);
	LCD_WR_DATA(0x04);
	LCD_WR_DATA(0x0E);
	LCD_WR_DATA(0x0F);
	LCD_WR_DATA(0x09);
	LCD_WR_DATA(0x07);
	LCD_WR_DATA(0x08);
	LCD_WR_DATA(0x03);

	LCD_WR_REG(0xE8);
	LCD_WR_DATA(0x34);

	LCD_WR_REG(0x62);
	LCD_WR_DATA(0x18);
	LCD_WR_DATA(0x0D);
	LCD_WR_DATA(0x71);
	LCD_WR_DATA(0xED);
	LCD_WR_DATA(0x70);
	LCD_WR_DATA(0x70);
	LCD_WR_DATA(0x18);
	LCD_WR_DATA(0x0F);
	LCD_WR_DATA(0x71);
	LCD_WR_DATA(0xEF);
	LCD_WR_DATA(0x70);
	LCD_WR_DATA(0x70);

	LCD_WR_REG(0x63);
	LCD_WR_DATA(0x18);
	LCD_WR_DATA(0x11);
	LCD_WR_DATA(0x71);
	LCD_WR_DATA(0xF1);
	LCD_WR_DATA(0x70);
	LCD_WR_DATA(0x70);
	LCD_WR_DATA(0x18);
	LCD_WR_DATA(0x13);
	LCD_WR_DATA(0x71);
	LCD_WR_DATA(0xF3);
	LCD_WR_DATA(0x70);
	LCD_WR_DATA(0x70);

	LCD_WR_REG(0x64);
	LCD_WR_DATA(0x28);
	LCD_WR_DATA(0x29);
	LCD_WR_DATA(0xF1);
	LCD_WR_DATA(0x01);
	LCD_WR_DATA(0xF1);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x07);

	LCD_WR_REG(0x66);
	LCD_WR_DATA(0x3C);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0xCD);
	LCD_WR_DATA(0x67);
	LCD_WR_DATA(0x45);
	LCD_WR_DATA(0x45);
	LCD_WR_DATA(0x10);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x00);

	LCD_WR_REG(0x67);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x3C);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x01);
	LCD_WR_DATA(0x54);
	LCD_WR_DATA(0x10);
	LCD_WR_DATA(0x32);
	LCD_WR_DATA(0x98);

	LCD_WR_REG(0x74);
	LCD_WR_DATA(0x10);
	LCD_WR_DATA(0x85);
	LCD_WR_DATA(0x80);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x4E);
	LCD_WR_DATA(0x00);

	LCD_WR_REG(0x98);
	LCD_WR_DATA(0x3e);
	LCD_WR_DATA(0x07);

	LCD_WR_REG(0x35);
	LCD_WR_REG(0x21);

	LCD_WR_REG(0x11);
	delay_ms(120);
	LCD_WR_REG(0x29);
	delay_ms(20);

	LCD_Clear(0);
	return true;
}

void video_deinit() {
}

void video_flip() {
	
}
#endif