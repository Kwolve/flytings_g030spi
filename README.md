# FlyThings_STM32G030

#### 介绍
STM32G030C8T6 SPI屏幕专用sdk；
FLASH接口：PA4 Flash_CS, SPI1
LCD接口： SPI2 ,DC PB13, CS, PB12 
其他接口没有要求，采购中科世为FlyThings Lite授权Flash即可使用，配套FlyThings Lite 开发工具。
开发网址https://lite.flythings.cn/； 工具
http://14731609.s21d-14.faiusrd.com/0/ABUIABBLGAAg6pntgAYoypr1rwI?f=FlyThingsLite-setup.exe&v=1612401900

商务联系：钟先生 13728931930

#### 软件架构
Core Drivers  来自STM32 CubeMX生成的代码
flyport GUI适配的代码，包括触控，显示屏，定时器
uifun  开始FlyThings Lite开发工具匹配的接口
user  板子端的用户代码，包括Flash的升级代码（强烈建议不要修改，否则导致PC工具无法通过串口烧录UI文件）


#### 安装教程

##### PC端
1.  安装开发工具：
2.  查看开发文档
3. 编译工具Keil 5 编译器5.06及以上。

##### 设备端
1. 通过STM32Pro 烧录FlyThingsLiteG030_SPI_SEC.hex 到芯片
2. 编译仓库代码烧录到设备端。
