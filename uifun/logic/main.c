#include "logic/main.h"
#include "input.h"
#include "ui_res_image.h"

const uint8_t ui0[]={
    UI_RES_IMAGE_01,
    UI_RES_IMAGE_02,
    UI_RES_IMAGE_03,
    UI_RES_IMAGE_04,
    UI_RES_IMAGE_05
};
const uint8_t ui1[]={
    UI_RES_IMAGE_201,
    UI_RES_IMAGE_202,
    UI_RES_IMAGE_203,
    UI_RES_IMAGE_204,
    UI_RES_IMAGE_205
};
const uint8_t ui3[]={
    UI_RES_IMAGE_301,
    UI_RES_IMAGE_302,
    UI_RES_IMAGE_303,
    UI_RES_IMAGE_304,
    UI_RES_IMAGE_305
};
const uint8_t ui4[]={
    UI_RES_IMAGE_401,
    UI_RES_IMAGE_402,
    UI_RES_IMAGE_403,
    UI_RES_IMAGE_404,
    UI_RES_IMAGE_405
};
const uint8_t ui5[]={
    UI_RES_IMAGE_501,
    UI_RES_IMAGE_502,
    UI_RES_IMAGE_503,
    UI_RES_IMAGE_504
};

typedef struct{
    uint8_t count;
    const uint8_t* img_tab;
}S_UIMENU;
const S_UIMENU menu[]={
    4,ui5,
    5,ui0,
    5,ui1,
    5,ui3,
    5,ui4
};
int menuindex = 0;
int subindex = 0;
#define MENU_MAX  4

void ui_proc_keyevent(S_INPUT_KEY* key){
    if(key->keystatus == KEYST_DOWN){
        switch(key->keyvalue){
            case KEY_LIGHT1:
                break;
            case KEY_LIGHT2:
                menuindex--;
                if(menuindex < 0)   menuindex = MENU_MAX;

                printf("set bg img %d:%d\n",menuindex,menu[menuindex].img_tab[0]);
                widget_set_bg_img((widget_t*)UI_WIDGET_MAIN_Image2,menu[menuindex].img_tab[0]);
                subindex = 0;
                break;
            case KEY_LIGHT3:
                menuindex++;
                if(menuindex > MENU_MAX)   menuindex = 0;

                printf("set bg img %d:%d\n",menuindex,menu[menuindex].img_tab[0]);
                widget_set_bg_img((widget_t*)UI_WIDGET_MAIN_Image2,menu[menuindex].img_tab[0]);
                subindex = 0;
                break;
            case KEY_ENCODE_NEXT:
                
                subindex++;
                if(subindex >= menu[menuindex].count){
                    subindex = 0;
                }
                widget_set_bg_img((widget_t*)UI_WIDGET_MAIN_Image2,menu[menuindex].img_tab[subindex]);


                break;
            case KEY_ENCODE_PRE:
                subindex--;
                if(subindex < 0){
                    subindex =menu[menuindex].count-1;
                }
                widget_set_bg_img((widget_t*)UI_WIDGET_MAIN_Image2,menu[menuindex].img_tab[subindex]);

                break;
        }
    }

}
void ui_main_on_page_open(uint8_t page) {
    
    widget_set_bg_img((widget_t*)UI_WIDGET_MAIN_Image2,menu[menuindex].img_tab[0]);
    subindex = 0;
}

/**
 * @brief 定时器回调接口
 * @param millisecond   定时器周期，单位 ms
 * @param count  累计次数
 */
void ui_main_on_timer(uint32_t millisecond, uint32_t count) {

}

/**
 * @brief 按钮点击事件回调接口
 * @param button   按钮控件指针
 * @param type     触摸事件类型
 */
void ui_main_on_button_touch_event(button_t *button, touch_type_e type) {

}

/**
 * @brief 进度条回调接口
 * @param progress  进度条控件指针
 * @param val       进度值
 */
void ui_main_on_progress_changed(progress_t *progress, uint16_t val) {

}

/**
 * @brief 动画播放结束回调接口
 * @param animation  动画控件指针
 */
void ui_main_on_animation_play_end(animation_t *animation) {

}
