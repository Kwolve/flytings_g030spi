/****************************************************************************
** 图片资源
**
** 每次编译项目将自动创建该文件
**
** 请勿手动修改文件内容
*****************************************************************************/

#ifndef _UI_RES_IMAGE_H_
#define _UI_RES_IMAGE_H_

#define UI_RES_IMAGE_01                             0
#define UI_RES_IMAGE_02                             1
#define UI_RES_IMAGE_03                             2
#define UI_RES_IMAGE_04                             3
#define UI_RES_IMAGE_05                             4
#define UI_RES_IMAGE_201                            5
#define UI_RES_IMAGE_202                            6
#define UI_RES_IMAGE_203                            7
#define UI_RES_IMAGE_204                            8
#define UI_RES_IMAGE_205                            9
#define UI_RES_IMAGE_301                            10
#define UI_RES_IMAGE_302                            11
#define UI_RES_IMAGE_303                            12
#define UI_RES_IMAGE_304                            13
#define UI_RES_IMAGE_305                            14
#define UI_RES_IMAGE_401                            15
#define UI_RES_IMAGE_402                            16
#define UI_RES_IMAGE_403                            17
#define UI_RES_IMAGE_404                            18
#define UI_RES_IMAGE_405                            19
#define UI_RES_IMAGE_501                            20
#define UI_RES_IMAGE_502                            21
#define UI_RES_IMAGE_503                            22
#define UI_RES_IMAGE_504                            23

#endif /*_UI_RES_IMAGE_H_*/
