# FlyThings_STM32G030

#### Description
STM32G030C8T6 SPI屏幕专用sdk；
FLASH接口：PA4 Flash_CS, SPI1
LCD接口： SPI2 ,DC PB13, CS, PB12 
其他接口没有要求，采购中科世为FlyThings Lite授权Flash即可使用，配套FlyThings Lite 开发工具。
开发网址https://lite.flythings.cn/； 工具
http://14731609.s21d-14.faiusrd.com/0/ABUIABBLGAAg6pntgAYoypr1rwI?f=FlyThingsLite-setup.exe&v=1612401900

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
